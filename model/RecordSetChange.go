package model

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/route53"
)


func BuildRoute53RecordUpdate(hostedZoneId string, recordSetIp string, recordSetName string, recordType string, ttl int) *route53.ChangeResourceRecordSetsInput {
	input := &route53.ChangeResourceRecordSetsInput{
		ChangeBatch: &route53.ChangeBatch{
			Changes: []*route53.Change{
				{
					Action: aws.String("UPSERT"),
					ResourceRecordSet: &route53.ResourceRecordSet{
						Name: aws.String(recordSetName),
						ResourceRecords: []*route53.ResourceRecord{
							{
								Value: aws.String(recordSetIp),
							},
						},
						TTL:  aws.Int64(int64(ttl)),
						Type: aws.String(recordType),
					},
				},
			},
			Comment: aws.String("Web server for example.com"),
		},
		HostedZoneId: aws.String(hostedZoneId),
	}
	return input
}