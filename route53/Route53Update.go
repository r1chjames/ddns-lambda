package route53

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/route53"
)

func ChangeRecordSet(record *route53.ChangeResourceRecordSetsInput) (*route53.ChangeResourceRecordSetsOutput, error) {

	sess, _ := session.NewSession(&aws.Config{
		Region: aws.String("eu-west-1")},
	)

	r53 := route53.New(sess, &aws.Config{
		LogLevel:    aws.LogLevel(aws.LogDebugWithHTTPBody),
	})

	return r53.ChangeResourceRecordSets(record)
}

