package main

import (
	"context"
	"os"
	"gitlab.com/r1chjames/ddns-lambda/model"
	"gitlab.com/r1chjames/ddns-lambda/route53"
	"encoding/json"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

// https://serverless.com/framework/docs/providers/aws/events/apigateway/#lambda-proxy-integration
type Response events.APIGatewayProxyResponse

// Handler is our lambda handler invoked by the `lambda.Start` function call
func Handler(ctx context.Context, req events.APIGatewayProxyRequest) (Response, error) {

	hostedZoneId := os.Getenv("HOSTED_ZONE_ID")
	recordType := os.Getenv("RECORD_TYPE")

	requestBody := parseRequestBody(req)
	response, err := route53.ChangeRecordSet(model.BuildRoute53RecordUpdate(
		hostedZoneId,
		requestBody.RecordSetIp,
		requestBody.RecordSetName,
		recordType,
		300))

	responseBody,_ := json.Marshal(response.ChangeInfo)

	if err != nil {
		resp := Response{
			StatusCode:      500,
			IsBase64Encoded: false,
			Body:            string(err.Error()),
			Headers: map[string]string{
				"Content-Type":           "application/json",
				"X-MyCompany-Func-Reply": "update-handler",
			},
		}
		return resp, nil
	} else {
		resp := Response{
			StatusCode:      204,
			IsBase64Encoded: false,
			Body:            string(responseBody),
			Headers: map[string]string{
				"Content-Type":           "application/json",
				"X-MyCompany-Func-Reply": "update-handler",
			},
		}
		return resp, nil
	}

}

func parseRequestBody(request events.APIGatewayProxyRequest ) model.HttpRequestType {
	var body model.HttpRequestType
	err := json.Unmarshal([]byte(request.Body), &body)
	if err != nil {
		panic(err)
	}
	return body
}

func main() {
	lambda.Start(Handler)
}